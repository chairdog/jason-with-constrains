package edu.uow.DSL.Jason.Constraint;

import edu.uow.DSL.Jason.Constraint.Parser.AnnotationParser;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.IParser;
import edu.uow.DSL.Jason.Constraint.Parser.Objective.IObjectiveParser;
import edu.uow.DSL.Jason.Constraint.Parser.Objective.Improve;
import edu.uow.DSL.Jason.Constraint.Parser.Objective.Maximize;
import edu.uow.DSL.Jason.Constraint.Parser.Objective.Minimize;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.IntVar2FloatVar;
import jason.JasonException;
import jason.architecture.AgArch;
import jason.asSemantics.Agent;
import jason.asSemantics.Event;
import jason.asSemantics.Option;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.floats.constraints.LinearFloat;
import org.jacop.floats.constraints.PminusCeqR;
import org.jacop.floats.core.FloatDomain;
import org.jacop.floats.core.FloatVar;
import org.jacop.floats.search.LargestDomainFloat;
import org.jacop.floats.search.Optimize;
import org.jacop.floats.search.SplitSelectFloat;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.PrintOutListener;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by yg452 on 8/06/16.
 */
public class ConstraintAgent extends Agent {
    protected int SOLVER_TIMEOUT = 10000;
    protected String varSource = null;
    Store constraintStore = null;
    //todo datafiles for store objectives
    private HashSet<Var> maximizingVars = null;
    private HashSet<Var> improvingVars = null;
    private HashSet<Var> minimizeVars = null;
    //    private LinkedList<HashSet<Constraint>> constraintStack = null;
    private AnnotationParser parser = null;

    public ConstraintAgent(String asSource) throws JasonException {
        super();
        initAg(asSource);
    }

    public ConstraintAgent() {
        super();
        initAg();
    }

    @Override
    public ConstraintAgent clone(AgArch arch) {
        try {
            ConstraintAgent agent = (ConstraintAgent) super.clone();
            agent.varSource = this.varSource;
            agent.maximizingVars = new HashSet<>(this.maximizingVars);
            agent.improvingVars = new HashSet<>(this.improvingVars);
            agent.minimizeVars = new HashSet<>(this.minimizeVars);
            // todo clone constraintStore
            // todo clone constraintStack
            throw new NotImplementedException();
//            return agent;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void initAg() {
        super.initAg();

        maximizingVars = new HashSet<>();
        improvingVars = new HashSet<>();
        minimizeVars = new HashSet<>();
        constraintStore = new Store();
//        constraintStack = new LinkedList<>();
        try {
            parser = new AnnotationParser();
            logger.fine(String.format("Found Parsers:\n%s", parser.parsers.keySet().toString()));
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw new Error("Cannot create constraint annotation parser" + e);
        }

        // load properties
        Properties prop = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
        double precision = 1E-7;
        if (inputStream != null) {
            try {
                prop.load(inputStream);
                this.SOLVER_TIMEOUT = Integer.parseInt(prop.getProperty("JaCoP.timeout"));
                precision = Double.parseDouble(prop.getProperty("JaCoP.Float.Precision"));
            } catch (IOException e) {
            }
        } else {
            // set JaCoP Floating point precision
            switch (System.getProperty("sun.arch.data.model")) {
                case "64":
                case "x64":
                case "amd64":
                case "xmd64":
                    precision = 1E-17;
                    break;
                default:
                case "86":
                case "32":
                case "x86":
                    precision = 1E-7;
                    break;
            }
        }
        logger.info("JaCoP: JVM version " + System.getProperty("sun.arch.data.model"));
        logger.info(String.format("JaCoP: Setting Solver Timeout to %d", this.SOLVER_TIMEOUT));
        FloatDomain.setPrecision(precision);
        logger.info(String.format("JaCoP: Setting Float Domain Precision to %e", precision));
    }

    @Override
    public void load(String asSrc) throws JasonException {
        super.load(asSrc);
        loadConstraintsFromBelief(true);

        if (!constraintStore.consistency()) {
            throw new JasonException("Inconsistent Constraints");
        }
    }

    private void loadConstraintsFromBelief(Boolean loadObjectives) throws JasonException {
        // load constraint variables from belief base
        constraintStore = new Store();
        Iterator<Literal> it = this.bb.iterator();
        if (it != null) {
            while (it.hasNext()) {
                Literal belief = it.next();
                Var var = null;
                try {
                    var = parser.parseConstraintVariable(constraintStore, belief);
                } catch (ParsingException e) {
                    //ignore
                }
                if (var != null) {
                    logger.fine(String.format("Added constraint variable %s", var.toString()));
                }
            }
        }

        // add initial objectives and constraints
        if (loadObjectives) {
            improvingVars.clear();
            maximizingVars.clear();
        }
        it = this.bb.iterator();
        if (it != null) {
            while (it.hasNext()) {
                Literal belief = it.next();
                updateObjectiveAndConstraint(belief, loadObjectives, true);
            }
        }
    }

    private void updateObjectiveAndConstraint(Literal belief, boolean allowObjectives, boolean allowConstraints) throws JasonException {
        IParser ocParser = parser.getParser(belief.getFunctor());
        if (ocParser != null) {
            if (allowConstraints && ocParser instanceof IConstraintParser) {
                IConstraintParser cParser = (IConstraintParser) ocParser;
                Constraint constraint = null;
                try {
                    constraint = cParser.parse(constraintStore, belief);
                } catch (ParsingException e) {
                    e.printStackTrace();
                }
                updateConstraint(constraint, belief.negated());
            } else if (allowObjectives && ocParser instanceof IObjectiveParser) {
                IObjectiveParser oParser = (IObjectiveParser) ocParser;
                List<Var> variables = null;
                try {
                    variables = oParser.parse(constraintStore, belief);
                    logger.fine(variables.toString());
                    if (oParser instanceof Maximize) {
                        for (Var var : variables) {
                            if (belief.negated()) {
                                // stop improving variable
                                this.maximizingVars.remove(var);
                                logger.fine(String.format("I'm no longer maximizing %s", var.toString()));
                            } else {
                                // start improving variable
                                this.maximizingVars.add(var);
                                logger.fine(String.format("I'm now maximizing %s", var.toString()));
                            }
                        }
                    } else if (oParser instanceof Improve) {
                        for (Var var : variables) {
                            if (belief.negated()) {
                                // stop improving variable
                                this.improvingVars.remove(var);
                                logger.fine(String.format("I'm no longer improving %s%s", var.id, var.dom().toString()));
                            } else {
                                // start improving variable
                                this.improvingVars.add(var);
                                logger.fine(String.format("I'm now improving %s%s", var.id, var.dom().toString()));
                            }
                        }
                    } else if (oParser instanceof Minimize) {
                        for (Var var : variables) {
                            if (belief.negated()) {
                                // stop improving variable
                                this.minimizeVars.remove(var);
                                logger.fine(String.format("I'm no longer minimizing %s%s", var.id, var.dom().toString()));
                            } else {
                                // start improving variable
                                this.minimizeVars.add(var);
                                logger.fine(String.format("I'm now minimizing %s%s", var.id, var.dom().toString()));
                            }
                        }
                    }
                } catch (ParsingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateConstraint(Constraint c, boolean isRemove) {
        if (c != null) {
            if (!isRemove) {
                constraintStore.impose(c);
                logger.fine(String.format("Constraint %s is added.", c.toString()));
            } else {
                constraintStore.getConstraints().remove(c); // todo not sure this is working.
                logger.fine(String.format("Constraint %s is removed.", c.toString()));
            }
        }
    }

    @Override
    public Event selectEvent(Queue<Event> events) {
        // select event and update optimization objectives
        Event event = super.selectEvent(events);
        if (event != null) {
            ListTerm annots = event.getTrigger().getLiteral().getAnnots();
            try {
                processConstraintAnnotations(annots, true, false);
            } catch (JasonException e) {
                e.printStackTrace();
            }
        }
        return event;
    }

    private void processConstraintAnnotations(ListTerm annots, boolean objectives, boolean constraints) throws JasonException {
        if (annots != null) {
            for (Term annot : annots) {
                if (annot.isLiteral()) {
                    updateObjectiveAndConstraint((Literal) annot, objectives, constraints);
                }
            }
        }
    }

    @Override
    public Option selectOption(List<Option> options) {
        Option bestOption = null;
        double bestCost = Double.MAX_VALUE;

        for (Option option : options) {
            try {
                loadConstraintsFromBelief(false);
                Plan plan = option.getPlan();
                ListTerm annots = plan.getLabel().getAnnots();
                try {
                    processConstraintAnnotations(annots, false, true);
                } catch (JasonException e) {
                    e.printStackTrace();
                }

                if (constraintStore.consistency()) {
                    // todo find the best option by minimize cost
                    double cost = calculateCost(option);
                    logger.info(String.format("(Cost=%f) Plan=%s",
                            cost, option.getPlan().toString()));
                    if (cost < bestCost) {
                        bestCost = cost;
                        bestOption = option;
                    }
                } else {
                    logger.warning("Constraints are inconsistent");
                }
            } catch (JasonException e) {
                e.printStackTrace();
            }
        }
        if (bestOption == null) {
            bestOption = super.selectOption(options); // fallback to the original agent option selection function
        }
        return bestOption;
    }

    private double calculateCost(Option option) {
        // this is a hack, not sure if it works
        if (constraintStore.consistency()) {
            if (improvingVars.isEmpty() && maximizingVars.isEmpty() && minimizeVars.isEmpty()) {
                return Double.MIN_VALUE;
            } else {
                //weight for normalize variables
                ArrayList<Double> weights = new ArrayList<>();
                ArrayList<FloatVar> normalizedVars = new ArrayList<>();
                logger.fine("Maximizing: " + maximizingVars.toString());
                for (Var var : maximizingVars) {
                    FloatVar fvar;
                    if (var instanceof FloatVar) {
                        fvar = (FloatVar) var;
                    } else {
                        fvar = IntVar2FloatVar.convert(constraintStore, var);
                    }
                    weights.add(1 / (fvar.min() - fvar.max()));
                    FloatVar newVar = new FloatVar(constraintStore, fvar.id + "_NORMALIZED_", fvar.min() - fvar.max(), 0.0);
                    normalizedVars.add(newVar);
                    constraintStore.impose(new PminusCeqR(fvar, fvar.max(), newVar));
                }

                logger.fine("Minimizing: " + minimizeVars.toString());
                for (Var var : minimizeVars) {
                    FloatVar fvar;
                }

                logger.fine("Improving: " + improvingVars.toString());
                for (Var var : improvingVars) {
                    // make the variable at least as good as the current preferred value, if the preferred value does not exists, then make it better than its minimal value
                    //todo implementation and revision
                }

                assert (weights.size() == normalizedVars.size());
//        logger.fine(constraintStore.variablesHashMap.toString());

                // cost
                FloatVar cost = new FloatVar(constraintStore, "_COST_", 0, normalizedVars.size());
                LinearFloat costConstraint = new LinearFloat(constraintStore, normalizedVars, weights, "==", cost.value());
                constraintStore.impose(costConstraint);

                //search

                // find all variable of interests
                ArrayList<FloatVar> floatVars = new ArrayList<>();
                for (Var var : constraintStore.variablesHashMap.values()) {
                    if (var.sizeConstraintsOriginal() > 0) {
                        if (var instanceof FloatVar) {
                            floatVars.add((FloatVar) var);
                        } else {
                            // find the float var equivalent
                            FloatVar fvar = (FloatVar) constraintStore.variablesHashMap.get(IntVar2FloatVar.getFloatName(var));
                            if (fvar != null) {
                                floatVars.add(fvar);
                            }
                        }
                    }
                }

                DepthFirstSearch<FloatVar> search = new DepthFirstSearch<FloatVar>();
                SplitSelectFloat<FloatVar> select = new SplitSelectFloat<FloatVar>(
                        constraintStore,
                        floatVars.toArray(new FloatVar[floatVars.size()]),
                        new LargestDomainFloat<>());
                search.setTimeOut(SOLVER_TIMEOUT);
                search.setSolutionListener(new PrintOutListener<FloatVar>());

//        search.getSolutionListener().searchAll(true);
                Optimize min = new Optimize(constraintStore, search, select, cost);
                boolean result = min.minimize();
//                result = search.labeling(constraintStore, select, cost);
                if (result)
                    return min.getFinalCost().max();
            }
        }
        return Double.MAX_VALUE;
    }
}

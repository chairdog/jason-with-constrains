package edu.uow.DSL.Jason.Constraint.Parser.Variable;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Term;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;

import java.util.List;

/**
 * Created by yg452 on 17/06/16.
 */
@AutoService(IVariableParser.class)
public class Int implements IVariableParser {
    static final String key = "int";

    @Override
    public Var parse(Store store, Literal l) throws ParsingException {
        Var var = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 3) {
                    throw new Exception();
                }
                Atom varName = (Atom) terms.get(0);
                NumberTerm min = (NumberTerm) terms.get(1);
                NumberTerm max = (NumberTerm) terms.get(2);
                var = new IntVar(store, varName.toString(), (int) min.solve(), (int) max.solve());
            } catch (Exception e) {
                throw new ParsingException(String.format("Definition of a integer constraint variable must be int(atom, min, max), where atom is the variable name (start with lower case) and min, max are numerical values. %s\n", l.toString()) + e);
            }
        }
        return var;
    }

    @Override
    public String getKey() {
        return key;
    }
}

package edu.uow.DSL.Jason.Constraint.Parser.Variable;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.IParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import jason.asSyntax.Literal;
import org.jacop.core.Store;
import org.jacop.core.Var;

/**
 * Created by yg452 on 17/06/16.
 */
@AutoService(IParser.class)
public interface IVariableParser extends IParser {
    public Var parse(Store store, Literal l) throws ParsingException;
}

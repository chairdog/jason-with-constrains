package edu.uow.DSL.Jason.Constraint.Parser.Variable;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.core.BooleanVar;
import org.jacop.core.Store;
import org.jacop.core.Var;

import java.util.List;

/**
 * Created by yg452 on 17/06/16.
 */
@AutoService(IVariableParser.class)
public class Boolean implements IVariableParser {
    static final String key = "boolean";

    @Override
    public Var parse(Store store, Literal l) throws ParsingException {
        Var var = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() == 1) {
                    Atom varName = (Atom) terms.get(0);
                    var = new BooleanVar(store, varName.getFunctor());
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                throw new ParsingException(String.format("Definition of a boolean constraint variable must be boolean(atom), where atom is the variable name (start with lower case). %s\n", l.toString()) + e);
            }
        }
        return var;
    }

    @Override
    public String getKey() {
        return key;
    }
}

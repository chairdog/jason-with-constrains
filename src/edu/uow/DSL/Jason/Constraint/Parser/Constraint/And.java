package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.AndBool;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Constraint that implements logic and operation on its arguments and return results
 * Jason syntax: and([], result), where the list can be either a list of Int/boolean variables or a list of constraints.
 */
@AutoService(IConstraintParser.class)
public class And implements IConstraintParser {
    static final String key = "and";
    static final String name = "And";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws JasonException, ParsingException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 2) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                ListTerm list = (ListTerm) terms.get(0);
                IntVar resultVar = Generic.parseIntVar(store, terms.get(1));
                if (list.get(0).isAtom()) {
                    //list of variables
                    ArrayList<IntVar> vars = Generic.parseIntVars(store, list);
                    constraint = new AndBool(vars, resultVar);
                } else {
                    //list of constraints
                    throw new NotImplementedException();
                }
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s([], result), where [] is a list of Integer, Boolean variables or constraints, and result is a constraint variable. %s\n", name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

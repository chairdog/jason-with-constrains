package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PdivCeqR;
import org.jacop.floats.constraints.PdivQeqR;

import java.util.List;

/**
 * This constraint implements X / Y = Z, which implementing the following 3 different constraints
 * + (IntVar) X / (IntVar) Y = (IntVar) Z (XdivYeqZ in JaCoP)
 * + (FloatVar) X / (FloatVar) Y = (FloatVar) Z (PdivQeqR in JaCoP)
 * + (FloatVar) X / Constant = (FloatVar) Z (PdivCeqR in JaCoP)
 * Syntax
 * ```Jason
 * div(X,Y,Z)
 * ```
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class XdivYeqZ implements IConstraintParser {
    static final String key = "div";
    static final String name = "XdivYeqZ";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createBinaryOperationConstraint(store, terms,
                        null,
                        null,
                        org.jacop.constraints.XdivYeqZ.class,
                        PdivCeqR.class,
                        null,
                        PdivQeqR.class);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

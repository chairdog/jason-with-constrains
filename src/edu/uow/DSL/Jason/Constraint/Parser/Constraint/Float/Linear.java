package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.IntVar2FloatVar;
import jason.JasonException;
import jason.asSyntax.*;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.floats.core.FloatVar;

import java.util.ArrayList;
import java.util.List;

/**
 * Linear constraint implements the weighted summation over several variables . sum(i in 1..N)(ai*xi) = b It provides the weighted sum from all variables on the list.
 * Jason:
 * linear([variables], [weights], "relation", var)
 * the relation, one of "==", "<", ">", "<=", ">=", "!=" (String)
 * Created by chairdog on 23/06/16.
 */
@AutoService(IConstraintParser.class)
public class Linear implements IConstraintParser {
    static final String name = "Linear";
    static final String key = "linear";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        boolean isIntVars = true;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 4) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                ListTerm list = (ListTerm) terms.get(0);
                ListTerm weithts = (ListTerm) terms.get(1);
                Atom relation = (Atom) terms.get(2);
                Var resultVar = Generic.parseVar(store, terms.get(3));
                if (resultVar instanceof FloatVar) {
                    isIntVars = false;
                }
                //list of variables
                ArrayList<Var> vars = new ArrayList<>();
                for (Term term : list) {
                    Var var = Generic.parseVar(store, term);
                    if (var instanceof FloatVar) {
                        isIntVars = false;
                    }
                    vars.add(var);
                }
                // list of weights
                ArrayList<Double> weightsDouble = new ArrayList<>();
                ArrayList<Integer> weightsInt = new ArrayList<>();
                for (Term term : weithts) {
                    NumberTerm num = (NumberTerm) term;
                    int iW = (int) num.solve();
                    double fW = num.solve();
                    weightsDouble.add(num.solve());
                    if (iW != fW) {
                        isIntVars = false;
                    }
                    weightsInt.add(iW);
                    weightsDouble.add(fW);
                }

                if (isIntVars) {
                    ArrayList<IntVar> intVars = new ArrayList<>();
                    for (Var v : vars) {
                        intVars.add((IntVar) v);
                    }
                    constraint = new org.jacop.constraints.LinearInt(store, intVars, weightsInt, relation.toString(), ((IntVar) resultVar).value());
                } else {
                    // convert all non floatvar to FloatVar
                    resultVar = IntVar2FloatVar.convert(store, resultVar);
                    ArrayList<FloatVar> floatVars = new ArrayList<>();
                    for (Var v : vars) {
                        floatVars.add(IntVar2FloatVar.convert(store, v));
                    }
                    constraint = new org.jacop.floats.constraints.LinearFloat(store, floatVars, weightsDouble, relation.toString(), ((FloatVar) resultVar).value());
                }
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s([variables],[weithgs],relations,result). %s\n", name, key, l.toString()) + e.toString());
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

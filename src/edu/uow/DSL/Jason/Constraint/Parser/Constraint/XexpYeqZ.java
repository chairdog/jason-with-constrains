package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Triple;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;

import java.util.List;

/**
 * Constraint X ^ Y #= Z (where all variables are integers). Syntax:
 * exp(X,Y,Z)
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class XexpYeqZ implements IConstraintParser {
    static final String key = "exp";
    static final String name = "XexpYeqZ";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            try {
                List<Term> terms = l.getTerms();
                if (terms.size() != 3) {
                    throw new Exception();
                }
                Triple<IntVar, IntVar, IntVar> vars = Generic.parseTriple(store, terms, IntVar.class, IntVar.class, IntVar.class);
                constraint = new org.jacop.constraints.XexpYeqZ(vars.x, vars.y, vars.z);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

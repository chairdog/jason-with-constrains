package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XplusClteqZ;
import org.jacop.core.Store;

import java.util.List;

/**
 * Constraint X + Y <= Z, IntVar only, which implements all of the following JaCoP
 * constraints
 * XplusClteqZ
 * XplusYlteqZ
 * where X, Y and Z are IntVars, and C is a constant.
 * Syntax
 * ```
 * pluslteq(X,Y,Z)
 * ```
 * Created by yg452 on 29/06/16.
 */
@AutoService(IConstraintParser.class)
public class XplusYlteqZ implements IConstraintParser {
    static final String name = "XplusYlteqZ";
    static final String key = "pluslteq";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createBinaryOperationConstraint(store, terms,
                        XplusClteqZ.class,
                        null,
                        org.jacop.constraints.XplusYlteqZ.class,
                        null,
                        null,
                        null);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Alldiff;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;

import java.util.ArrayList;
import java.util.List;

/**
 * Constraint that assures all the variables has different values.
 * Jason syntax: allDifferent(v1,...)
 */
@AutoService(IConstraintParser.class)
public class AllDifferent implements IConstraintParser {
    final static String key = "allDifferent";
    final static String name = "AllDifferent";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws JasonException, ParsingException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                ArrayList<IntVar> vars = Generic.parseIntVars(store, l.getTerms());
                if (vars.isEmpty()) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                constraint = new Alldiff(vars); // use a slightly more efficient version of Alldifferent
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s(atom, ...), where atoms are the integer constraint variable names (start with lower case). %s\n", name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

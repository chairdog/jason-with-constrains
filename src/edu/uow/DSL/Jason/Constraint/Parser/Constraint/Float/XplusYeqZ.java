package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XplusCeqZ;
import org.jacop.constraints.XplusYeqC;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PplusCeqR;
import org.jacop.floats.constraints.PplusQeqR;

import java.util.List;

/**
 * Constraint X + Y = Z, which implements the following JaCoP
 * constraints and select them accordingly,
 * XplusYeqC
 * XplusYeqZ
 * XplusCeqZ
 * PplusCeqR
 * PplusQeqR
 * where X, Y and Z are IntVar, C is a constant, and P, Q and R are FloatVar
 * Syntax:
 * ```
 * plus(X,Y,Z)
 * ```
 * Created by yg452 on 29/06/16.
 */
@AutoService(IConstraintParser.class)
public class XplusYeqZ implements IConstraintParser {
    static final String name = "XplusYeqZ";
    static final String key = "plus";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createBinaryOperationConstraint(store, terms,
                        XplusCeqZ.class,
                        XplusYeqC.class,
                        org.jacop.constraints.XplusYeqZ.class,
                        PplusCeqR.class,
                        null,
                        PplusQeqR.class);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XmulCeqZ;
import org.jacop.constraints.XmulYeqC;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PmulCeqR;
import org.jacop.floats.constraints.PmulQeqR;

import java.util.List;

/**
 * Constraint X * Y = Z, which implements all of the following constraints in JaCoP
 * XmulCeqZ, where X and Z are IntVar, and C is a constant.
 * XmulYeqC, where X and Y are IntVar, and C is a constant.
 * XmulYeqC, where X, Y and Z are IntVar.
 * PmulCeqR, where P and R are FloatVar, and C is a constant.
 * PmulQeqR, where P, Q and R are FloatVar
 * Syntax:
 * ```
 * mul(X,Y,Z)
 * ```
 * Created by chairdog on 6/28/2016.
 */
@AutoService(IConstraintParser.class)
public class XmulYeqZ implements IConstraintParser {
    static final String name = "XmulYeqZ";
    static final String key = "mul";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createBinaryOperationConstraint(store, terms,
                        XmulCeqZ.class,
                        XmulYeqC.class,
                        XmulYeqZ.class,
                        PmulCeqR.class,
                        null,
                        PmulQeqR.class);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

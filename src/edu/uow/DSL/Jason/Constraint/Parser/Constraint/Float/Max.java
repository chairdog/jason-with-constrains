package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.IntVar2FloatVar;
import jason.JasonException;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.floats.core.FloatVar;

import java.util.ArrayList;
import java.util.List;

/**
 * ```
 * Jason:
 * max([], maxVariable)
 * Created by chairdog on 23/06/16.
 */
@AutoService(IConstraintParser.class)
public class Max implements IConstraintParser {
    static final String name = "Max";
    static final String key = "max";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        boolean isIntVars = true;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 2) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                ListTerm list = (ListTerm) terms.get(0);
                Var resultVar = Generic.parseVar(store, terms.get(1));
                if (resultVar instanceof FloatVar) {
                    isIntVars = false;
                }
                ArrayList<Var> vars = new ArrayList<>();
                if (isIntVars) {
                    for (Term term : list) {
                        Var var = Generic.parseVar(store, term);
                        if (var instanceof FloatVar) {
                            isIntVars = false;
                            break;
                        }
                        vars.add(var);
                    }
                }
                if (isIntVars) {
                    ArrayList<IntVar> intVars = new ArrayList<>();
                    for (Var v : vars) {
                        intVars.add((IntVar) v);
                    }
                    constraint = new org.jacop.constraints.Max(intVars, (IntVar) resultVar);
                } else {
                    // convert all non floatvar to FloatVar
                    resultVar = IntVar2FloatVar.convert(store, resultVar);
                    ArrayList<FloatVar> floatVars = new ArrayList<>();
                    for (Var v : vars) {
                        floatVars.add(IntVar2FloatVar.convert(store, v));
                    }
                    constraint = new org.jacop.floats.constraints.Max(floatVars, (FloatVar) resultVar);
                }
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s([], result), where [] is a list of constraint variables, and result is a constraint variable. %s\n", name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

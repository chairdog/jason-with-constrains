package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.IParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import jason.JasonException;
import jason.asSyntax.Literal;
import org.jacop.constraints.Constraint;
import org.jacop.core.Store;

/**
 * Created by yg452 on 17/06/16.
 */
@AutoService(IParser.class)
public interface IConstraintParser extends IParser {
    String getName();

    Constraint parse(Store store, Literal l) throws ParsingException, JasonException;
}

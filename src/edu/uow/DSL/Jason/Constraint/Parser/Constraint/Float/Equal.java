package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XeqC;
import org.jacop.constraints.XeqY;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PeqC;
import org.jacop.floats.constraints.PeqQ;
import org.jacop.floats.constraints.XeqP;

import java.util.List;

/**
 * This constraint implements X = y, which could be one of the following implementation in JaCoP
 * XeqC(X,C), where X is IntVar and C is a constant
 * PeqC(X,C), where X is a FloatVar and C is a constant
 * XeqY(X,Y), where X is a IntVar and Y is a IntVar
 * XeqP(X,Y), where X is a IntVar and Y is a FloatVar
 * PeqQ(X,Y), where X is a FloatVar and Y is a FloatVar
 * Syntax:
 * ```Jason
 * eq(X,Y)
 * ```
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class Equal implements IConstraintParser {
    static final String key = "eq";
    static final String name = "XeqY";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createPairwiseConstraint(store, terms,
                        XeqC.class,
                        XeqY.class,
                        PeqC.class,
                        PeqQ.class,
                        XeqP.class);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }

        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

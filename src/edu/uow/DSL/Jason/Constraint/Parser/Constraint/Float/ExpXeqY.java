package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.Store;
import org.jacop.floats.constraints.ExpPeqR;

import java.util.List;

/**
 * Constraint exp(X) = Y
 * syntax:
 * ```
 * expXeqY(X,Y)
 * ```
 * Created by yg452 on 29/06/16.
 */
@AutoService(IConstraintParser.class)
public class ExpXeqY implements IConstraintParser {
    static final String name = "ExpXeqY";
    static final String key = "expXeqY";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createPairwiseConstraint(store, terms,
                        null,
                        null,
                        null,
                        ExpPeqR.class,
                        null);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

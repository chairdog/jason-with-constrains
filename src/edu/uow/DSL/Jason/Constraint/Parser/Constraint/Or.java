package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.OrBool;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements logic or operation on its arguments and return results
 * Jason syntax: or([],result), where the list is a list of Int/boolean variables
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class Or implements IConstraintParser {
    static final String key = "or";
    static final String name = "Or";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 2) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                ListTerm list = (ListTerm) terms.get(0);
                IntVar resultVar = Generic.parseIntVar(store, terms.get(1));
                if (list.get(0).isAtom()) {
                    //list of variables
                    ArrayList<IntVar> vars = Generic.parseIntVars(store, list);
                    constraint = new OrBool(vars, resultVar);
                } else {
                    //list of constraints
                    throw new NotImplementedException();
                }
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s([], result), where [] is a list of Integer, Boolean variables or constraints, and result is a constraint variable. %s\n", name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

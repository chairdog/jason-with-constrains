package edu.uow.DSL.Jason.Constraint.Parser.Constraint;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;

import java.util.ArrayList;
import java.util.List;

/**
 * ArgMax constraint provides the index of the maximum variable from all variables on the list.
 * Jason:
 * argMax([], maxIndex)
 * Created by chairdog on 23/06/16.
 */
@AutoService(IConstraintParser.class)
public class ArgMax implements IConstraintParser {
    static final String name = "ArgMax";
    static final String key = "argMax";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                if (terms.size() != 2) {
                    throw new ArrayIndexOutOfBoundsException();
                }
                ArrayList<IntVar> vars = Generic.parseIntVars(store, (ListTerm) terms.get(0));
                IntVar resultVar = Generic.parseIntVar(store, terms.get(1));
                constraint = new org.jacop.constraints.ArgMax(vars, resultVar);
            } catch (ArrayIndexOutOfBoundsException | ClassCastException e) {
                throw new ParsingException(String.format("Definition of a %s constraint must be %s([], result), where [] is a list of Integer or Boolean variables, and result is a integer constraint variable. %s\n", name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

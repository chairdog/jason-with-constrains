package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.Store;
import org.jacop.floats.constraints.AbsPeqR;

import java.util.List;

/**
 * Constraint |X| = Y (both Int and Float variables), syntax in Jason absXeqY(x,y)
 */
@AutoService(IConstraintParser.class)
public class AbsXeqY implements IConstraintParser {
    static final String key = "absXeqY";
    static final String name = "AbsXeqY";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createPairwiseConstraint(store, terms,
                        null,
                        org.jacop.constraints.AbsXeqY.class,
                        null,
                        AbsPeqR.class,
                        null);
            }
//            catch (ArrayIndexOutOfBoundsException | ClassCastException e){
            catch (Exception e) {
                throw new ParsingException(String.format(
                        "Definition of a %s constraint must be %s(atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

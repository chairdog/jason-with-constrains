package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XlteqC;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PlteqC;
import org.jacop.floats.constraints.PlteqQ;

import java.util.List;

/**
 * Constraint X <= Y, which could be one of the following implementation in JaCoP
 * XlteqC(X,C), where X is an IntVar, and C is a constant
 * XlteqY(X,Y), where X, and Y are IntVar
 * PlteqC(P,C), where P is a FloatVar and C is a constant
 * PlteqQ(P,Q), where P and Q are FloatVar
 * Syntax:
 * ```
 * lteq(X,Y)
 * ```
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class XlteqY implements IConstraintParser {
    static final String key = "lteq";
    static final String name = "XlteqY";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createPairwiseConstraint(store, terms,
                        XlteqC.class,
                        org.jacop.constraints.XlteqY.class,
                        PlteqC.class,
                        PlteqQ.class,
                        null);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

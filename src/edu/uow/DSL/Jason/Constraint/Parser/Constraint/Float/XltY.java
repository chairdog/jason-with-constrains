package edu.uow.DSL.Jason.Constraint.Parser.Constraint.Float;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import edu.uow.DSL.Jason.Constraint.Parser.Utility.Generic;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.constraints.XltC;
import org.jacop.core.Store;
import org.jacop.floats.constraints.PltC;
import org.jacop.floats.constraints.PltQ;

import java.util.List;

/**
 * Constraint X < Y, which could be one of the following implementation in JaCop
 * XltC(X,C), where X is an IntVar, and C is a constant
 * XltY(X,Y), where X, and Y are IntVar
 * PltC(P,C), where P is a FloatVar and C is a constant
 * PltQ(P,Q), where P and Q are FloatVar
 * Syntax:
 * ```
 * lt(X,Y)
 * ```
 * Created by chairdog on 6/27/2016.
 */
@AutoService(IConstraintParser.class)
public class XltY implements IConstraintParser {
    static final String key = "lt";
    static final String name = "XltY";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Constraint parse(Store store, Literal l) throws ParsingException, JasonException {
        Constraint constraint = null;
        if (key.equals(l.getFunctor())) {
            List<Term> terms = l.getTerms();
            try {
                constraint = Generic.createPairwiseConstraint(store, terms,
                        XltC.class,
                        org.jacop.constraints.XltY.class,
                        PltC.class,
                        PltQ.class,
                        null);
            } catch (Exception e) {
                throw new ParsingException(String.format(
                        "A %s constraint must be %s(atom, atom), where atoms are the constraint variable names (start with lower case). %s\n",
                        name, key, l.toString()) + e);
            }
        }
        return constraint;
    }

    @Override
    public String getKey() {
        return key;
    }
}

package edu.uow.DSL.Jason.Constraint.Parser.Objective;

import com.google.auto.service.AutoService;

/**
 * Created by yg452 on 22/06/16.
 */
@AutoService(IObjectiveParser.class)
public class Minimize implements IObjectiveParser {
    static final String key = "minimize";

    @Override
    public String getKey() {
        return key;
    }
}

package edu.uow.DSL.Jason.Constraint.Parser.Objective;

import com.google.auto.service.AutoService;

/**
 * Improve variable objective, syntax in Jason improve(v1,...)
 */
@AutoService(IObjectiveParser.class)
public class Improve implements IObjectiveParser {
    static final String key = "improve";

    @Override
    public String getKey() {
        return key;
    }
}

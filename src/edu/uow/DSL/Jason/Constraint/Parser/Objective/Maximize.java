package edu.uow.DSL.Jason.Constraint.Parser.Objective;

import com.google.auto.service.AutoService;

/**
 * Maximize variable objective, syntax in Jason maximize(v1,...)
 */
@AutoService(IObjectiveParser.class)
public class Maximize implements IObjectiveParser {
    static final String key = "maximize";

    @Override
    public String getKey() {
        return key;
    }
}

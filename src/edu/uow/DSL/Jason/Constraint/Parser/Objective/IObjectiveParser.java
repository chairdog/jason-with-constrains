package edu.uow.DSL.Jason.Constraint.Parser.Objective;

import com.google.auto.service.AutoService;
import edu.uow.DSL.Jason.Constraint.Parser.IParser;
import edu.uow.DSL.Jason.Constraint.Parser.ParsingException;
import jason.JasonException;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import org.jacop.core.Store;
import org.jacop.core.Var;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by yg452 on 17/06/16.
 */
@AutoService(IParser.class)
public interface IObjectiveParser extends IParser {
    default List<Var> parse(Store store, Literal l) throws JasonException, ParsingException {
        List<Var> vars = null;
        if (getKey().equals(l.getFunctor())) {
            vars = new LinkedList<>();
            for (Term term : l.getTerms()) {
                try {
                    Atom atom = (Atom) term;
                    Var var = store.variablesHashMap.get(atom.toString());
                    if (var == null) {
                        throw new JasonException("Unknown constraint variable " + atom);
                    }
                    vars.add(var);
                } catch (ClassCastException e) {
                    throw new ParsingException(String.format(
                            "%s objective must be in the form %s(v1,...), where the arguments must be constraint variable name. (Found %s in %s)",
                            getKey(), getKey(), term.toString(), l.toString()));
                }
            }
        }
        return vars;
    }
}

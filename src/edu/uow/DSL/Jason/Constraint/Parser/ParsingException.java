package edu.uow.DSL.Jason.Constraint.Parser;

/**
 * Created by yg452 on 17/06/16.
 */
public class ParsingException extends Exception {
    public ParsingException(String s) {
        super(s);
    }
}

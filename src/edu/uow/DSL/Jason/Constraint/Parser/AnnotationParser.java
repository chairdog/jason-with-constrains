package edu.uow.DSL.Jason.Constraint.Parser;

import com.google.common.collect.Sets;
import edu.uow.DSL.Jason.Constraint.Parser.Constraint.IConstraintParser;
import edu.uow.DSL.Jason.Constraint.Parser.Objective.IObjectiveParser;
import edu.uow.DSL.Jason.Constraint.Parser.Variable.IVariableParser;
import jason.asSyntax.Literal;
import org.jacop.core.Store;
import org.jacop.core.Var;

import java.util.Arrays;
import java.util.HashMap;
import java.util.ServiceLoader;

/**
 * Created by yg452 on 8/06/16.
 */
public class AnnotationParser {
    final HashMap<String, IVariableParser> variableParsers = loadParser(IVariableParser.class);
    final HashMap<String, IConstraintParser> constraintParsers = loadParser(IConstraintParser.class);
    final HashMap<String, IObjectiveParser> objectiveParsers = loadParser(IObjectiveParser.class);
    public HashMap<String, IParser> parsers = null;

    public AnnotationParser() throws InstantiationException, IllegalAccessException {
        parsers = new HashMap<>();
        parsers.putAll(variableParsers);
        Sets.SetView<String> intersection = Sets.intersection(parsers.keySet(), constraintParsers.keySet());
        if (!intersection.isEmpty()) {
            new Error(String.format("Parsers for key %s has been defined", Arrays.toString(intersection.toArray())));
        }
        parsers.putAll(constraintParsers);
        intersection = Sets.intersection(parsers.keySet(), objectiveParsers.keySet());
        if (!intersection.isEmpty()) {
            new Error(String.format("Parsers for key %s has been defined", Arrays.toString(intersection.toArray())));
        }
        parsers.putAll(objectiveParsers);
    }

    private static <S extends IParser> HashMap<String, S> loadParser(Class<S> parserType) throws IllegalAccessException, InstantiationException {
        HashMap<String, S> parsers = new HashMap<String, S>();
        ServiceLoader<S> loader = ServiceLoader.load(parserType);
        for (S parser : loader) {
            String key = parser.getKey();
            if (parsers.containsKey(key)) {
                throw new Error(String.format("Parser for key %s has been defined.", key));
            }
            parsers.put(key, parser);
        }
        return parsers;
    }

    public IParser getParser(String s) {
        return parsers.get(s);
    }

    public Var parseConstraintVariable(Store store, Literal belief) throws ParsingException {
        Var var = null;
        IVariableParser parser = variableParsers.get(belief.getFunctor());
        if (parser != null) {
            var = parser.parse(store, belief);
        } else {
            throw new ParsingException(String.format("Unknown constraint variable type %s", belief.toString()));
        }
        return var;
    }
}

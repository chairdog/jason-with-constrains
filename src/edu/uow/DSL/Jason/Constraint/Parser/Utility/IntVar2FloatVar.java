package edu.uow.DSL.Jason.Constraint.Parser.Utility;

import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.floats.constraints.XeqP;
import org.jacop.floats.core.FloatVar;

/**
 * Created by chairdog on 23/06/16.
 */
public class IntVar2FloatVar {
    final static String PREFIX = "_INTVAR_";

    /**
     * return the float var id that is equivalent to the id of an IntVar
     *
     * @param v
     * @return
     */
    public static String getFloatName(Var v) {
        if (v instanceof IntVar) {
            return PREFIX + v.id;
        }
        return v.id;
    }

    public static FloatVar convert(Store store, Var v) {
        if (v instanceof IntVar) {
            FloatVar fvar = (FloatVar) store.variablesHashMap.get(getFloatName(v));
            if (fvar == null) {
                // create new floatvar
                IntVar iVar = (IntVar) v;
                fvar = new FloatVar(store, getFloatName(v), iVar.min(), iVar.max());
                store.impose(new XeqP(iVar, fvar));
            }
            return fvar;
        }
        return (FloatVar) v;
    }
}

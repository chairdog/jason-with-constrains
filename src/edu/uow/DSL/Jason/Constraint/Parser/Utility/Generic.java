package edu.uow.DSL.Jason.Constraint.Parser.Utility;

import jason.JasonException;
import jason.asSyntax.Atom;
import jason.asSyntax.ListTerm;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Term;
import org.jacop.constraints.Constraint;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.core.Var;
import org.jacop.floats.core.FloatVar;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chairdog on 6/27/2016.
 */
public class Generic {
    static public Triple smartParseTriple(Store store, List<Term> terms) throws JasonException, IndexOutOfBoundsException {
        Triple triple = new Triple();
        triple.x = smartParseUni(store, terms.get(0));
        triple.y = smartParseUni(store, terms.get(1));
        triple.z = smartParseUni(store, terms.get(2));
        return triple;
    }

    static public Pair smartParsePair(Store store, List<Term> t) throws JasonException, IndexOutOfBoundsException {
        Pair pair = new Pair();
        pair.x = smartParseUni(store, t.get(0));
        pair.y = smartParseUni(store, t.get(1));
        return pair;
    }

    static public Object smartParseUni(Store store, Term t) throws JasonException {
        try {
            return new Double(parseDouble(store, t));
        } catch (ClassCastException e) {
            return parseVar(store, t);
        }
    }

    static public <A, B, C> Triple<A, B, C> parseTriple(Store store, List<Term> terms, Class<A> typeX, Class<B> typeY, Class<C> typeZ) throws JasonException, IndexOutOfBoundsException {
        Triple<A, B, C> triple = new Triple<A, B, C>();
        triple.assign(parsePair(store, terms, typeX, typeY));
        triple.z = parseUni(store, terms.get(2), typeZ);
        return triple;
    }

    static public <A, B> Pair<A, B> parsePair(Store store, List<Term> terms, Class<A> typeX, Class<B> typeY) throws JasonException, IndexOutOfBoundsException {
        Pair<A, B> pair = new Pair<A, B>();
        pair.x = parseUni(store, terms.get(0), typeX);
        pair.y = parseUni(store, terms.get(1), typeY);
        return pair;
    }

    static public <A> A parseUni(Store store, Term t, Class<A> typeX) throws JasonException {
        if (typeX == IntVar.class) {
            return (A) parseIntVar(store, t);
        } else if (typeX == FloatVar.class) {
            return (A) parseFloatVar(store, t);
        } else if (typeX == Double.class) {
            return (A) new Double(parseDouble(store, t));
        }
        return null;
    }

    static public double parseDouble(Store store, Term t) throws JasonException, ClassCastException {
        double d = ((NumberTerm) t).solve();
        return d;
    }

    static public Var parseVar(Store store, Term t) throws JasonException, ClassCastException {
        Atom atom = (Atom) t;
        Var var = store.variablesHashMap.get(atom.toString());
        if (var == null) {
            throw new JasonException("unknown constraint variable " + atom.getFunctor());
        }
        return var;
    }

    static public IntVar parseIntVar(Store store, Term t) throws JasonException, ClassCastException {
        Var var = parseVar(store, t);
        return (IntVar) var;
    }

    static public FloatVar parseFloatVar(Store store, Term t) throws JasonException, ClassCastException {
        Var var = parseVar(store, t);
        return IntVar2FloatVar.convert(store, var);
    }

    static public ArrayList<IntVar> parseIntVars(Store store, List<Term> lt) throws JasonException, ClassCastException {
        ArrayList<IntVar> vars = new ArrayList<>();
        for (Term term : lt) {
            vars.add(parseIntVar(store, term));
        }
        return vars;
    }

    static public Constraint createPairwiseConstraint(Store store, List<Term> terms, Class XC, Class XY, Class PC, Class PQ, Class XP) throws JasonException {
        Constraint constraint = null;
        if (terms.size() != 2) {
            throw new IndexOutOfBoundsException();
        }
        Pair pair = smartParsePair(store, terms);
        try {
            if (XY != null && pair.x instanceof IntVar && pair.y instanceof IntVar) {
                constraint = (Constraint) XY.getConstructor(IntVar.class, IntVar.class).newInstance(pair.x, pair.y);
            } else if (pair.y instanceof Double) {
                int yi = ((Double) pair.y).intValue();
                double y = ((Double) pair.y).doubleValue();
                if (XC != null && pair.x instanceof IntVar && y == yi) {
                    constraint = (Constraint) XC.getConstructor(IntVar.class, int.class).newInstance(pair.x, yi);
                } else {
                    if (PC != null) {
                        constraint = (Constraint) PC.getConstructor(FloatVar.class, double.class).newInstance(IntVar2FloatVar.convert(store, (Var) pair.x), y);
                    } else {
                        throw new JasonException(String.format("Constraint with the variable type (%s,%s) not supported by JaCoP.", pair.x.getClass().getName(), pair.y.getClass().getName()));
                    }
                }
            } else {
                if (XP != null && pair.x instanceof IntVar && pair.y instanceof FloatVar) {
                    constraint = (Constraint) XP.getConstructor(IntVar.class, FloatVar.class).newInstance(pair.x, pair.y);
                } else if (PQ != null) {
                    constraint = (Constraint) PQ.getConstructor(FloatVar.class, FloatVar.class)
                            .newInstance(IntVar2FloatVar.convert(store, (Var) pair.x), IntVar2FloatVar.convert(store, (Var) pair.y));
                } else {
                    throw new JasonException(String.format("Constraint with the variable type (%s,%s) not supported by JaCoP.", pair.x.getClass().getName(), pair.y.getClass().getName()));
                }
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return constraint;
    }

    static public Constraint createBinaryOperationConstraint(Store store, List<Term> terms, Class XCZ, Class XYC, Class XYZ, Class PCR, Class PQC, Class PQR) throws JasonException {
        Constraint constraint = null;
        if (terms.size() != 3) {
            throw new IndexOutOfBoundsException();
        }
        Triple triple = smartParseTriple(store, terms);
        try {
            if (XYZ != null && triple.x instanceof IntVar && triple.y instanceof IntVar && triple.z instanceof IntVar) {
                constraint = (Constraint) XYZ.getConstructor(IntVar.class, IntVar.class, IntVar.class).newInstance(triple.x, triple.y, triple.z);
            } else if (triple.x instanceof Var && triple.y instanceof Double && triple.z instanceof Var) {
                int yi = ((Double) triple.y).intValue();
                double y = ((Double) triple.y).doubleValue();
                if (XCZ != null && triple.x instanceof IntVar && y == yi && triple.z instanceof IntVar) {
                    constraint = (Constraint) XCZ.getConstructor(IntVar.class, int.class, IntVar.class).newInstance(triple.x, yi, triple.z);
                } else {
                    if (PCR != null) {
                        constraint = (Constraint) PCR.getConstructor(FloatVar.class, double.class, FloatVar.class).newInstance(triple.x, y, triple.z);
                    } else {
                        throw new JasonException(String.format("Constraint with the variable type (%s,%s,%s) not supported by JaCoP.", triple.x.getClass().getName(), triple.y.getClass().getName(), triple.z.getClass().getName()));
                    }
                }
            } else if (triple.x instanceof Var && triple.y instanceof Var && triple.z instanceof Double) {
                int zi = ((Double) triple.z).intValue();
                double z = ((Double) triple.z).doubleValue();
                if (XYC != null && triple.x instanceof IntVar && triple.y instanceof IntVar && z == zi) {
                    constraint = (Constraint) XYC.getConstructor(IntVar.class, IntVar.class, int.class).newInstance(triple.x, triple.y, zi);
                } else {
                    if (PQC != null) {
                        constraint = (Constraint) PQC.getConstructor(FloatVar.class, FloatVar.class, double.class).newInstance(triple.x, triple.y, z);
                    } else {
                        throw new JasonException(String.format("Constraint with the variable type (%s,%s,%s) not supported by JaCoP.", triple.x.getClass().getName(), triple.y.getClass().getName(), triple.z.getClass().getName()));
                    }
                }
            } else {
                if (PQR != null) {
                    constraint = (Constraint) PQR.getConstructor(FloatVar.class, FloatVar.class, FloatVar.class)
                            .newInstance(IntVar2FloatVar.convert(store, (Var) triple.x),
                                    IntVar2FloatVar.convert(store, (Var) triple.y),
                                    IntVar2FloatVar.convert(store, (Var) triple.z));
                } else {
                    throw new JasonException(String.format("Constraint with the variable type (%s,%s,%s) not supported by JaCoP.", triple.x.getClass().getName(), triple.y.getClass().getName(), triple.z.getClass().getName()));
                }
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return constraint;
    }


    public static ArrayList<Var> parseVars(Store store, ListTerm terms) throws JasonException {
        ArrayList<Var> vars = new ArrayList<>();
        for (Term term : terms) {
            vars.add(parseVar(store, term));
        }
        return vars;
    }
}

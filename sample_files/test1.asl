// Agent Test1 in project custAgent.mas2j

/* Initial beliefs and rules */
float(x, -1.0, 1.0).
int(y, 0, 100).
int(int1, -1, 1).
int(int2, 1, 2131).
boolean(z).
boolean(bool1).
boolean(bool2).

gt(int2, int1).

improve(x).

/* Initial goals */

!start [~improve(x,z), maximize(y)].

/* Plans */

@p1 [gt(x,0), absXeqY(int1,y)] // this annotation indicates the constraints that has to be satisificated at the end of the plan execution.
+!start : true <- .print("p1 is selected").

@p2 [eq(x,y), eq(int1, 1)]
+!start : true <- .print("p2 is selected").
